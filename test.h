#ifndef _TEST_H_
#define _TEST_H_

typedef unsigned int uint32_t;

static inline void __panic_assert3(const char* file, int line, const char *c_a, uint32_t v_a,
									const char *op, const char *c_b, uint32_t v_b) {
    printf("user panic: %s:%d\n", file, line);
    printf("assertion failed: %s %s %s\n", c_a, op, c_b);
    printf("%s=%08x, %s=%08x\n", c_a, v_a, c_b, v_b);
    exit(0);
}

#define ASSERT3(a, op, b) \
	((a op b) ? (void)0 : __panic_assert3(__FILE__, __LINE__, #a, a, #op, #b, b))

#define ASSERT(b) assert(b)

#ifndef NULL
#define NULL 0
#endif

#endif /* _TEST_H_ */
