/*
 * Copyright (C) 2012 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _IDHASHTABLE_H_
#define _IDHASHTABLE_H_

#define DEFINE_IDHT(name)												\
	typedef struct __idht_##name##_node {								\
		struct __idht_##name##_node *prev;								\
		struct __idht_##name##_node *next;								\
		struct __idht_##name *ht;										\
	} idhtn_##name;														\
	/* */																\
	typedef struct __idht_##name {										\
		idhtn_##name** table;											\
		unsigned int table_size;										\
		unsigned int count;												\
	} idht_##name;														\
	/* */																\
	static inline void idht_##name##_init(idht_##name *t, void *mem, unsigned int mem_size) { \
		t->count = 0;													\
		t->table = (idhtn_##name**)mem;									\
		t->table_size = mem_size/sizeof(idhtn_##name*);					\
	}

#define DEFINE_IDHT_FUNCTIONS(fn_type,name,type_t)						\
	static inline type_t * idhtn_##name##_value(idhtn_##name *n);		\
	static inline unsigned int idhtn_##name##_id(idhtn_##name *n);		\
	/* */																\
	fn_type int															\
	idht_##name##_put(idht_##name *t, idhtn_##name *n) {				\
		if (n->ht)														\
			return -1;													\
		unsigned int hash = idhtn_##name##_id(n) % t->table_size;		\
		idhtn_##name *i = t->table[hash];								\
		if (i != NULL) {												\
			/* ayni hashe sahip eleman varsa oncesine ekle */			\
			n->next = i;												\
			i->prev = n;												\
		}																\
		t->table[hash] = n;												\
		n->ht = t;														\
		t->count++;														\
		return 0;														\
	}																	\
	/* */																\
	fn_type type_t*														\
	idht_##name##_get(idht_##name *t, unsigned int id) {				\
		unsigned int hash = id % t->table_size;							\
		idhtn_##name *i = t->table[hash];								\
		while (i != NULL) {												\
			if (idhtn_##name##_id(i) == id)								\
				return idhtn_##name##_value(i);							\
			i = i->next;												\
		}																\
		return NULL;													\
	}																	\
	/* */																\
	fn_type void														\
	idht_##name##_remove(idhtn_##name *n) {								\
		idht_##name *ht = n->ht;										\
		if (n->prev) {													\
			/* ilk eleman degilse */									\
			n->prev->next = n->next;									\
		} else {														\
			unsigned int hash = idhtn_##name##_id(n) % ht->table_size;	\
			ht->table[hash] = n->next;									\
		}																\
		if (n->next)													\
			n->next->prev = n->prev;									\
		n->ht = NULL;													\
		n->prev = n->next = NULL;										\
		ht->count--;													\
	}

#define DEFINE_IDHT_OFFSET(name, type_t, nn, idn)						\
	static inline type_t * idhtn_##name##_value(idhtn_##name *n) {		\
		return (type_t*)((char*)n-offsetof(type_t,nn));					\
	}																	\
	static inline unsigned int idhtn_##name##_id(idhtn_##name *n) {		\
		return *(unsigned int*)((char*)n-(offsetof(type_t,nn) - offsetof(type_t,idn))); \
	}

#endif /* _IDHASHTABLE_H_ */
