/*
 * Copyright (C) 2012 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stddef.h>

#include "../test.h"
#include "idhashtable.h"

DEFINE_IDHT(deneme)

typedef struct test {
	int a;
	int id;
	idhtn_deneme hash_node;
	int b;
} test_t;

DEFINE_IDHT_OFFSET(deneme, test_t, hash_node, id)
DEFINE_IDHT_FUNCTIONS(extern, deneme, test_t)

int main(int argc, char *const argv[]) {
	int i;
	int r = 0;
	test_t t[100];
	char hash_memory[100];

	idht_deneme ht;
	memset(hash_memory, 0, 100);
	idht_deneme_init(&ht, hash_memory, 100);

	ASSERT(idht_deneme_get(&ht, 0) == NULL);
	ASSERT(idht_deneme_get(&ht, 1234) == NULL);

	for (i = 0 ; i < 100 ; i++) {
		t[i].id = i;
		t[i].a = i + 2000;
		t[i].b = i + 4000;
		memset(&t[i].hash_node, 0, sizeof(t[i].hash_node));

		r = idht_deneme_put(&ht, &t[i].hash_node);
		ASSERT3(r, == , 0);
		r = idht_deneme_put(&ht, &t[i].hash_node);
		ASSERT3(r, == , -1);
	}

	ASSERT3( ht.count, ==, 100);

	for (i = 0 ; i < 100 ; i++) {
		ASSERT3(idht_deneme_get(&ht, i)->id, ==, i);
		ASSERT3(idht_deneme_get(&ht, i)->a, ==, i + 2000);
		ASSERT3(idht_deneme_get(&ht, i)->b, ==, i + 4000);
	}

	idht_deneme_remove(&idht_deneme_get(&ht, 10)->hash_node);
	ASSERT( t[10].hash_node.prev == NULL);
	ASSERT( t[10].hash_node.next == NULL);
	ASSERT( idht_deneme_get(&ht, 10) == NULL);
	ASSERT3( ht.count, ==, 99);

	idht_deneme_remove(&idht_deneme_get(&ht, 15)->hash_node);
	ASSERT( t[15].hash_node.prev == NULL);
	ASSERT( t[15].hash_node.next == NULL);
	ASSERT( idht_deneme_get(&ht, 15) == NULL);

	idht_deneme_remove(&idht_deneme_get(&ht, 99)->hash_node);
	ASSERT( t[99].hash_node.prev == NULL);
	ASSERT( t[99].hash_node.next == NULL);
	ASSERT( idht_deneme_get(&ht, 99) == NULL);

	for (i = 1 ; i < 100 ; i += ht.table_size) {
		idht_deneme_remove(&idht_deneme_get(&ht, i)->hash_node);
		ASSERT( t[i].hash_node.prev == NULL);
		ASSERT( t[i].hash_node.next == NULL);
		ASSERT( idht_deneme_get(&ht, i) == NULL);
	}

	for (i = 98 ; i > 0 ; i -= ht.table_size) {
		idht_deneme_remove(&idht_deneme_get(&ht, i)->hash_node);
		ASSERT( t[i].hash_node.prev == NULL);
		ASSERT( t[i].hash_node.next == NULL);
		ASSERT( idht_deneme_get(&ht, i) == NULL);
	}

	/* hepsini sil */
	for (i = 0 ; i < ht.table_size ; i++) {
		while (ht.table[i]) {
			idht_deneme_remove(ht.table[i]);
		}
	}
	ASSERT3( ht.count, ==, 0);

    /* ayni listede bulunan elemanlari silme */
    test_t x[5];
    for (i = 0 ; i < 5 ; i++) {
		memset(&x[i].hash_node, 0, sizeof(x[i].hash_node));
        x[i].id = i * 1000;
		r = idht_deneme_put(&ht, &x[i].hash_node);
		ASSERT(r == 0);
    }

	ASSERT( ht.count == 5 );

	/* ortadan silme */
	idht_deneme_remove(&x[2].hash_node);
	ASSERT( x[2].hash_node.ht == NULL );
	idht_deneme_remove(&x[3].hash_node);
	ASSERT( x[3].hash_node.ht == NULL );
	ASSERT( ht.count == 3 );
	/* bastan silme */
	idht_deneme_remove(&x[4].hash_node);
	ASSERT( x[4].hash_node.ht == NULL );
	/* sondan silme */
	idht_deneme_remove(&x[0].hash_node);
	ASSERT( x[0].hash_node.ht == NULL );
	/* son elemani silme */
	idht_deneme_remove(&x[1].hash_node);
	ASSERT( x[1].hash_node.ht == NULL );
	assert( ht.count == 0 );

	printf("test bitti\n");

	return 0;
}
