/*
 * malloc-free doubly linked list
 * Copyright (C) 2012 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _LIST_H_
#define _LIST_H_

typedef struct list_node {
	struct list_node *prev;
	struct list_node *next;
	struct list *list;
} li_node_t;

typedef struct list {
	unsigned int size;
	struct list_node __;
} list_t;

static inline void list_init(list_t *list) {
	list->size = 0;
	list->__.next = &list->__;
	list->__.prev = &list->__;
	list->__.list = list;
}

static inline int list_insert(li_node_t *pos, li_node_t *n) {
	if (n->list)
		return -1;
	n->list = pos->list;

	n->next = pos;
	n->prev = pos->prev;
	n->prev->next = n;
	pos->prev = n;

	n->list->size++;
	return 0;
}

static inline li_node_t* list_erase(li_node_t *pos) {
	li_node_t *pos_next = pos->next;
	list_t *pos_list = pos->list;

	if (pos == &pos_list->__)
		return NULL;

	pos->prev->next = pos_next;
	pos_next->prev = pos->prev;

	pos_list->size--;

	pos->list = NULL;
	pos->prev = pos->next = NULL;
	return pos_next;
}

static inline li_node_t* list_begin(list_t *list) {
	return list->__.next;
}

static inline li_node_t* list_end(list_t *list) {
	return &list->__;
}

static inline int list_push_front(list_t *list, li_node_t *n) {
	return list_insert(list_begin(list), n);
}

static inline int list_push_back(list_t *list, li_node_t *n) {
	return list_insert(list_end(list), n);
}

static inline int list_pop_front(list_t *list) {
	return -(list_erase(list_begin(list))==NULL);
}

static inline int list_pop_back(list_t *list) {
	return -(list_erase(list_end(list)->prev)==NULL);
}

#define DEFINE_LIST_OFFSET(listname,type_t,nodename)				\
	static inline type_t* li_##listname##_value(li_node_t *n) {		\
		return (type_t*)((char*)n - offsetof(type_t, nodename));	\
	}																\
	static inline type_t* li_##listname##_front(list_t *l) {		\
		return li_##listname##_value(l->__.next);					\
	}																\
	static inline type_t* li_##listname##_back(list_t *l) {			\
		return li_##listname##_value(l->__.prev);					\
	}

#endif /* _LIST_H_ */
