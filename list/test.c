/*
 * malloc-free doubly linked list tests
 * Copyright (C) 2012 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>

#ifdef LIST2_TEST
#include "list2.h"
#define list_t li_deneme
#define li_node_t li_deneme_node
#define list_init li_deneme_init
#define list_insert li_deneme_insert
#define list_erase li_deneme_erase
#define list_begin li_deneme_begin
#define list_end li_deneme_end
#define list_push_front li_deneme_push_front
#define list_push_back li_deneme_push_back
#define list_pop_front li_deneme_pop_front
#define list_pop_back li_deneme_pop_back
# else
#include "list.h"
#endif

#include "../test.h"

#ifdef LIST2_TEST
DEFINE_LIST(deneme)
DEFINE_LIST(list2)
#endif

typedef struct {
	int x;
	char y;
	li_node_t li_node;
	int z;
#ifdef LIST2_TEST
	li_list2_node li2_node;
#endif
} type;

DEFINE_LIST_OFFSET(deneme, type, li_node);
#ifdef LIST2_TEST
DEFINE_LIST_OFFSET(list2, type, li2_node);
#endif

int main(int argc, char *const argv[]) {
	int i;
	int r;
	list_t list;
#ifdef LIST2_TEST
	li_list2 list2;
#endif
	type t[10] = { { 0 } };
	li_node_t *pos = NULL;

	for (i = 0 ; i < 10 ; i++) {
		t[i].x = i;
	}

	list_init(&list);
#ifdef LIST2_TEST
	printf("list2 test\n");
	li_list2_init(&list2);
#endif
	ASSERT3(list.size, ==, 0);

	r = list_pop_front(&list);
	ASSERT3(r, ==, -1);
	r = list_pop_back(&list);
	ASSERT3(r, ==, -1);

	r = list_insert(list_begin(&list), &t[6].li_node);
	ASSERT3(r, >, -1);
	ASSERT3(list.size, ==, 1);
	ASSERT(li_deneme_front(&list) == li_deneme_back(&list));
	ASSERT(li_deneme_front(&list) == &t[6]);
	ASSERT(t[6].li_node.next == &list.__);
	ASSERT(t[6].li_node.list == &list);

	r = list_insert(list_begin(&list), &t[6].li_node);
	ASSERT3(r, ==, -1);

	r = list_insert(list_begin(&list), &t[3].li_node);
	ASSERT3(r, >, -1);
	ASSERT3(list.size, ==, 2);
	ASSERT(li_deneme_front(&list) == &t[3]);
	ASSERT(li_deneme_back(&list) == &t[6]);

	r = list_insert(list_begin(&list), &t[2].li_node);
	ASSERT3(r, >, -1);
	ASSERT3(list.size, ==, 3);
	ASSERT(li_deneme_front(&list) == &t[2]);
	ASSERT(li_deneme_back(&list) == &t[6]);

	/* __ elemani silinemez */
	pos = list_erase(list_end(&list));
	ASSERT(pos == NULL);

	/* eleman silindiginde return degeri bir sonraki eleman */
	pos = list_erase(list_end(&list)->prev);
	ASSERT(pos == &list.__);
	ASSERT3(list.size, ==, 2);

	r = list_insert(list_end(&list), &t[6].li_node);
	ASSERT3(r, >, -1);
	ASSERT3(list.size, ==, 3);

	pos = list_erase(&t[3].li_node);
	ASSERT(pos == &t[6].li_node);
	ASSERT(t[3].li_node.list == NULL);
	ASSERT(t[3].li_node.next == NULL);
	ASSERT(t[3].li_node.prev == NULL);

	r = list_insert(pos, &t[3].li_node);
	ASSERT3(r, >, -1);

	r = list_insert(pos, &t[4].li_node);
	ASSERT3(r, >, -1);

	r = list_insert(pos, &t[5].li_node);
	ASSERT3(r, >, -1);

	list_push_front(&list, &t[1].li_node);
	list_push_front(&list, &t[0].li_node);

	list_push_back(&list, &t[7].li_node);
	list_push_back(&list, &t[8].li_node);
	r = list_pop_back(&list);
	ASSERT3(r, >, -1);
	ASSERT(t[8].li_node.list == NULL);
	list_push_back(&list, &t[8].li_node);
	list_push_back(&list, &t[9].li_node);


#ifdef LIST2_TEST
	for (i = 0 ; i < 10 ; i++) {
		li_list2_push_back(&list2, &t[i].li2_node);
	}
	ASSERT3(list2.size, ==, 10);
#endif

	ASSERT3(list.size, ==, 10);

	i = 0;
	li_node_t *n;
	for (n = list_begin(&list) ; n != list_end(&list) ; n = n->next) {
		ASSERT3(li_deneme_value(n)->x, ==, i);
		i++;
	}

	i = 10;
	for (n = list_end(&list)->prev ; n != list_end(&list) ; n = n->prev) {
		i--;
		ASSERT3(li_deneme_value(n)->x, ==, i);
	}

	for (i = 0 ; i < 10 ; i++) {
		ASSERT3(li_deneme_front(&list)->x, ==, i);
		ASSERT3(li_deneme_value(list_begin(&list))->x, ==, i);
		r = list_pop_front(&list);
		ASSERT3(r, >, -1);
	}

	ASSERT3(list.size, ==, 0);
#ifdef LIST2_TEST
	ASSERT3(list2.size, ==, 10);
	i = 0;
	while (list2.size) {
		ASSERT3(li_list2_front(&list2)->x, ==, i);
		li_list2_pop_front(&list2);
		i++;
	}
	ASSERT3(list2.size, ==, 0);
#endif

	r = list_pop_back(&list);
	ASSERT3(r, <, 0);
	r = list_pop_front(&list);
	ASSERT3(r, <, 0);

	ASSERT3(list.size, ==, 0);

	printf("list test OK\n");
	return 0;
}
